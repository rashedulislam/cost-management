<?php
require 'db.php';

$month = date('m');
$year = date('Y');

$query = "SELECT cat_name,SUM(cost_amount) as column_sum FROM tbl_cost inner join tbl_cat on tbl_cost.cat_id=tbl_cat.cat_id WHERE month(cost_date)=:month && year(cost_date)=:year GROUP BY cat_name";
$stmt = $connection->prepare($query);
$stmt->execute([':month' => $month, ':year' => $year]);
$data = $stmt->fetchAll(PDO::FETCH_OBJ);

if (isset($_POST['submit'])) {
    $month = $_POST['month'];
    $year = $_POST['year'];

    $query = "SELECT cat_name,SUM(cost_amount) as column_sum FROM tbl_cost inner join tbl_cat on tbl_cost.cat_id=tbl_cat.cat_id WHERE month(cost_date)=:month && year(cost_date)=:year GROUP BY cat_name";
    $stmt = $connection->prepare($query);
    $stmt->execute([':month' => $month, ':year' => $year]);
    $data = $stmt->fetchAll(PDO::FETCH_OBJ);
}

$cat_name = array();
$column_sum = array();

foreach ($data as $value) {
    $cat_name[] = $value->cat_name;
    $column_sum[] = $value->column_sum;
}

// if (isset($_GET['jsondata'])) {
//     $a = $b = array();
//     foreach ($data as $d) {
//         $a[] = $d['cat_name'];
//         $b[] = $d['column_sum'];
//     }
//     echo json_encode(array('cat' => $a, 'cost' => $b));
//     exit();
// }

?>

<?php include 'header.php';?>

<h1>Search for any month expences category wise</h1>
<div>
  <form action="" method="post">
        <div>
            <label for="">Month:</label>
            <input value="<?=$month;?>" type="text" name="month">
        </div>
        <div>
            <label for="">Year:</label>
            <input value="<?=$year;?>" type="text" name="year">
        </div>
        <div>
            <input type="submit" name="submit" value="Search">
        </div>
  </form>
</div>

<h1>Expences of the month <?php echo $month . ', ' . $year; ?></h1>
<div>
    <table border="1">
        <thead>
            <tr>
                <th>Category Name</th>
                <th>Amount</th>
            </tr>
        </thead>
        <?php
foreach ($data as $value) {
    ?>
        <tr>
            <td><?php echo $value->cat_name; ?></td>
            <td><?php echo $value->column_sum; ?></td>
        </tr>
        <?php }?>
    </table>
</div>

<!-- code for charts -->


<div class="container">
    <canvas id="myChart"></canvas>
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($cat_name); ?>,
        datasets: [{
            label: 'All category cost',
            data: <?php echo json_encode($column_sum); ?>,
            backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(54, 162, 235, 0.6)',
            'rgba(255, 206, 86, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(153, 102, 255, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        }]
    },

    // Configuration options go here
    options: {}
});

</script>

<?php include 'footer.php';?>