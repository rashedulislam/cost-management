<?php

require 'db.php';

$cost_date = $_GET['cost_date'];
$query = "SELECT cat_name, SUM(cost_amount) as column_sum FROM tbl_cost inner join tbl_cat on tbl_cost.cat_id = tbl_cat.cat_id where cost_date =:cost_date GROUP BY cat_name";
$stmt = $connection->prepare($query);
$stmt->execute([':cost_date' => $cost_date]);
$data = $stmt->fetchAll(PDO::FETCH_OBJ);

if (isset($_POST['submit'])) {
    $cost_date = $_POST['cost_date'];

    $query = "SELECT cat_name, SUM(cost_amount) as column_sum FROM tbl_cost inner join tbl_cat on tbl_cost.cat_id = tbl_cat.cat_id where cost_date =:cost_date GROUP BY cat_name";
    $stmt = $connection->prepare($query);
    $stmt->execute([':cost_date' => $cost_date]);
    $data = $stmt->fetchAll(PDO::FETCH_OBJ);
}

$cat_name = array();
$column_sum = array();

foreach ($data as $value) {
    $cat_name[] = $value->cat_name;
    $column_sum[] = $value->column_sum;
}

?>

<?php include 'header.php';?>

<div>
    <h2>Search for a Single day Cost</h2>
    <form action="" method="post">
        <div>
            <label for="">Year:</label>
            <input value="<?php echo $cost_date; ?>" type="date" name="cost_date">
        </div>
        <div>
            <input type="submit" name="submit" value="Search">
        </div>
     </form>
</div>
<div>
    <h2>Expences of <?php echo $cost_date; ?></h2>
    <table>
        <thead>
            <tr>
                <th>Category Name</th>
                <th>Cost Amount</th>
            </tr>
        </thead>
        <?php
foreach ($data as $value) {
    ?>
            <tr>
                <td><?php echo $value->cat_name; ?></td>
                <td><?php echo $value->column_sum; ?></td>
            </tr>

        <?php }?>
    </table>
</div>

<!-- code for charts -->


<div class="container">
    <canvas id="myChart"></canvas>
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($cat_name); ?>,
        datasets: [{
            label: 'Cost of a Date',
            data: <?php echo json_encode($column_sum); ?>,
            backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(54, 162, 235, 0.6)',
            'rgba(255, 206, 86, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(153, 102, 255, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        }]
    },

    // Configuration options go here
    options: {}
});

</script>

<?php include 'footer.php';?>