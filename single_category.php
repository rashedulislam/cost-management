<?php
require 'db.php';

$cat_id = $_GET['cat_id'];
$month = date('m');
$year = date('Y');

$sql = 'SELECT * from tbl_cost WHERE cat_id=:cat_id && month(cost_date)=:month && year(cost_date)=:year';
$stmt = $connection->prepare($sql);
$stmt->execute([':cat_id' => $cat_id, ':month' => $month, ':year' => $year]);
$values = $stmt->fetchAll(PDO::FETCH_OBJ);

$sql = 'SELECT * from tbl_cat';
$stmt = $connection->prepare($sql);
$stmt->execute();
$catagories = $stmt->fetchAll(PDO::FETCH_OBJ);

if (isset($_GET['submit'])) {
    $cat_id = $_GET['cat_id'];
    $sql = "SELECT * from tbl_cost WHERE cat_id=:cat_id && month(cost_date)=:month && year(cost_date)=:year";
    $stmt = $connection->prepare($sql);
    $stmt->execute([':cat_id' => $cat_id, ':month' => $month, ':year' => $year]);
    $values = $stmt->fetchAll(PDO::FETCH_OBJ);
}

$cost_date = array();
$cost_amount = array();

foreach ($values as $value) {
    $cost_date[] = $value->cost_date;
    $cost_amount[] = $value->cost_amount;
}

?>

<?php include 'header.php';?>

<a href="allCategory.php">Search All Category</a> <br><br>

<div>
  <form action="" method="get">
    <select name="cat_id">
      <option>-- select category --</option>
      <?php foreach ($catagories as $category): ?>
        <option value="<?=$category->cat_id;?>"> <?=$category->cat_name;?></option>
      <?php endforeach;?>
    </select>
    <input type="submit" name="submit" value="Search" />
  </form>
</div>
<br><br>
<div>
  <table border="1">
    <thead>
      <tr>
        <th>Cost Name</th>
        <th>Cost Details</th>
        <th>Cost Amount</th>
        <th>Cost Date</th>
      </tr>
    </thead>
    <?php
foreach ($values as $value) {
    ?>
      <tr>
        <td><?=$value->cost_name;?></td>
        <td><?=$value->cost_details;?></td>
        <td><?=$value->cost_amount;?></td>
        <td><?=$value->cost_date;?></td>
      </tr>
    <?php }?>
  </table>
</div>

<!-- code for charts -->


<div class="container">
    <canvas id="myChart"></canvas>
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($cost_date); ?>,
        datasets: [{
            label: 'Single catagory cost',
            data: <?php echo json_encode($cost_amount); ?>,
            backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(54, 162, 235, 0.6)',
            'rgba(255, 206, 86, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(153, 102, 255, 0.6)',
            'rgba(255, 159, 64, 0.6)',
            'rgba(255, 99, 132, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        }]
    },

    // Configuration options go here
    options: {}
});

</script>

<?php include 'footer.php';?>